import { useEffect, useState } from "react";
import { useSocketIO, ReadyState } from "react-use-websocket";
function App() {
  const [message, setMessage] = useState("");
  const { sendMessage, lastMessage, readyState } = useSocketIO(
    "ws://127.0.0.1:4001",
    {
      onMessage: (event) => {
        setMessage(event.data);
      },
    }
  );

  return (
    <div className="App">
      Ready: {ReadyState[readyState]}
      <div>
        <button
          onClick={() => {
            sendMessage('42["hello",{"hello":"world"}]');
          }}
        >
          SEND MESSAGE TO SERVER
        </button>
      </div>
      <div>
        <div>
          <div>RECEIVED: {message}</div>
        </div>
      </div>
    </div>
  );
}

export default App;
